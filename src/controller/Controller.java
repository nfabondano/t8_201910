package controller;

import java.awt.BorderLayout;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import model.vo.*;
import model.data_structures.*;
import model.logic.ManejoFechaHora;
import model.logic.MapManager;
import view.MapManagerView;
import view.MapVisual;




public class Controller {

	public final static String MAP = "./info/map.xml";

	public final static String OPEN = "./info/Central-WashingtonDC-OpenStreetMap.xml";

	public final static String JSONOPEN = "./info/jsonOpen.json";

	public final static String JSONMAPV = "./info/jsonMapVert.json";
	
	public final static String JSONMAPA = "./info/jsonMapArcs.json";

	// Componente vista (consola)
	private MapManagerView view;

	// Componente modelo (logica de la aplicacion)
	private MapManager model;

	/**
	 * Metodo constructor
	 */
	public Controller()
	{
		view = new MapManagerView();
		model = new MapManager();
	}

	/**
	 * Metodo encargado de ejecutar los  requerimientos segun la opcion indicada por el usuario
	 */
	public void run(){

		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while(!fin){
			view.printMenu();
			int option = sc.nextInt();

			switch(option){

			case 0:
				try {
					model.loadMap(OPEN);
				} catch (Exception e) {
					view.printMessage("Error en cargar el mapa");
					e.printStackTrace();
				}
				break;
			case 1:
				model.writeJson(JSONOPEN);
				break;
			case 2:
				model.readJson(JSONOPEN);
				break;
			case 3:
				final MapVisual lel = new MapVisual(model.getMap());
				JFrame frame = new JFrame("Polylines");

				frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
				frame.add(lel, BorderLayout.CENTER);
				frame.setSize(700, 500);
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
				int option2 = sc.nextInt();
				break;
			case 4:
				try {
					model.loadMap(MAP);
				} catch (Exception e) {
					view.printMessage("Error en cargar el mapa");
					e.printStackTrace();
				}
				break;
			case 5:
				model.writeJsonVertex(JSONMAPV);
				model.writeJsonArcs(JSONMAPA);
				
				break;
			case 6:	
				fin = true;
				sc.close();
				break;
			}
		}
	}

}
