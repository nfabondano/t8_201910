package model.logic;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.IdentityHashMap;
import java.util.Iterator;

import com.opencsv.CSVReader;

import model.data_structures.Graph;
import model.data_structures.HashTableLP;
import model.data_structures.HashTableSC;
import model.data_structures.ITablaHash;
import model.data_structures.ORArray;
import model.data_structures.Pair;
import model.data_structures.ParVertices;
import model.vo.Coordinates;

import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

import java.util.*;
import java.io.*;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MapManager extends DefaultHandler{

	private Graph<Long, Coordinates, Pair<Double,Long> > mapa;

	private boolean way;

	private boolean highway;

	private Long idTempArco;

	private ORArray<Long> idVertices;

	/**
	 * Metodo constructor
	 */
	public MapManager()
	{

	}

	/**
	 * Cargar las infracciones de un semestre de 2018
	 * @param numeroSemestre numero del semestre a cargar (1 o 2)
	 * @return objeto con el resultado de la carga de las infracciones
	 */
	public void loadMap( String fileName ) throws Exception{
		mapa = new Graph<Long, Coordinates, Pair<Double,Long> >();

		SAXParserFactory spf = SAXParserFactory.newInstance();
		spf.setNamespaceAware(true);
		SAXParser saxParser = spf.newSAXParser();
		XMLReader xmlReader = saxParser.getXMLReader();
		xmlReader.setContentHandler(this);
		xmlReader.parse(convertToFileURL(fileName));

	}

	public String convertToFileURL(String filename) {
		String path = new File(filename).getAbsolutePath();
		if (File.separatorChar != '/') {
			path = path.replace(File.separatorChar, '/');
		}

		if (!path.startsWith("/")) {
			path = "/" + path;
		}
		return "file:" + path;
	}

	public void startDocument() throws SAXException {
		way = false; highway = false;
	}

	public void endDocument() throws SAXException {
		System.out.println("Vertices " + mapa.V());
		System.out.println("Arcos " + mapa.E());
		System.out.println();
	}

	public void startElement(String namespaceURI, String localName, String qName,  Attributes atts) throws SAXException {
		switch (qName) {
		case "node":
			Long id = Long.parseLong(atts.getValue(0));
			Double lat = Double.parseDouble(atts.getValue(1));
			Double lon = Double.parseDouble(atts.getValue(2));
			mapa.addVertex(id, new Coordinates(lat, lon));
			break;
		case "way":
			if(!way) {
				way = true;
				idTempArco = Long.parseLong(atts.getValue(0));
				idVertices = new ORArray<Long>(10);
			}
			break;
		case "nd":
			if(way) {
				Long idVertice = Long.parseLong(atts.getValue(0));
				idVertices.add(idVertice);
			}
			break;
		case "tag":
			if(atts.getValue(0).equalsIgnoreCase("highway") && way)
				highway = true;
			break;
		}
	}

	public void endElement (String uri, String localName, String qName) {	
		if(qName.equalsIgnoreCase("way") && way) {
			way = false;
			if(highway) {
				Long idTempVertice = idVertices.getElement(0);
				for(int i=1; i<idVertices.getSize(); i++) {
					Long idVertice = idVertices.getElement(i);
					Coordinates p1 = mapa.getInfoVertex(idVertice);
					Coordinates p2 = mapa.getInfoVertex(idTempVertice);
					Double dif = difference(p1, p2);
					Pair<Double,Long> par = new Pair<Double, Long>(dif,idTempArco);
					mapa.addEdge(idTempVertice, idVertice, par);
					idTempVertice = idVertice;
				}
				idVertices = new ORArray<Long>();
				highway = false;
			}
		}
	}

	public void writeJson( String fileName ) {

		JSONObject jsonMap = new JSONObject();

		JSONArray vertices = new JSONArray();
		Iterator<Long> it1 = mapa.vertices();
		while(it1.hasNext()) {
			Long id1 = it1.next();
			Coordinates coor = mapa.getInfoVertex(id1);

			JSONObject vertice = new JSONObject();
			JSONObject detVertice = new JSONObject();

			detVertice.put("id",id1);
			detVertice.put("lat", coor.lat);
			detVertice.put("lon", coor.lon);

			vertice.put("vertice", detVertice);

			vertices.add(vertice);
		}
		jsonMap.put("vertices", vertices);

		JSONArray arcos = new JSONArray();
		Iterator<ParVertices<Long>> it2 = mapa.arcos();
		while(it2.hasNext()) {
			ParVertices<Long> ids = it2.next();
			Pair<Double,Long> info = mapa.getInfoArc(ids.idV1, ids.idV2);

			JSONObject arco = new JSONObject();
			JSONObject detArco = new JSONObject();

			detArco.put("id", info.second);
			detArco.put("dist", info.first);
			detArco.put("idV1", ids.idV1);
			detArco.put("idV2", ids.idV2);

			arco.put("arco", detArco);

			arcos.add(arco);
		}
		jsonMap.put("arcos", arcos);

		try (FileWriter file = new FileWriter(fileName)) {

			file.write(jsonMap.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writeJsonVertex( String fileNameVer) {
		JSONObject jsonMap = new JSONObject();
		JSONArray vertices = new JSONArray();

		Iterator<Long> it = mapa.vertices();
		while(it.hasNext()) {

			Long id = it.next();	
			while(!mapa.adjVertex(id)) {
				id = it.next();
			}

			Coordinates coor = mapa.getInfoVertex(id);
			JSONObject vertice = new JSONObject();
			JSONObject detVertice = new JSONObject();

			detVertice.put("id",id);
			detVertice.put("lat", coor.lat);
			detVertice.put("lon", coor.lon);

			vertice.put("vertice", detVertice);
			vertices.add(vertice);
		}

		jsonMap.put("vertices", vertices);

		try (FileWriter file = new FileWriter(fileNameVer)) {

			file.write(jsonMap.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writeJsonArcs ( String fileName ) {
		JSONObject jsonMap = new JSONObject();
		JSONArray arcos = new JSONArray();
		Iterator<ParVertices<Long>> it2 = mapa.arcos();
		while(it2.hasNext()) {
			ParVertices<Long> ids = it2.next();
			Pair<Double,Long> info = mapa.getInfoArc(ids.idV1, ids.idV2);

			JSONObject arco = new JSONObject();
			JSONObject detArco = new JSONObject();

			detArco.put("id", info.second);
			detArco.put("dist", info.first);
			detArco.put("idV1", ids.idV1);
			detArco.put("idV2", ids.idV2);

			arco.put("arco", detArco);

			arcos.add(arco);
		}
		jsonMap.put("arcos", arcos);

		try (FileWriter file = new FileWriter(fileName)) {

			file.write(jsonMap.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void readJson( String fileName ) {
		Graph<Long,Coordinates,Pair<Double,Long>> copia = mapa;
		mapa = new Graph<Long, Coordinates, Pair<Double,Long>>();
		try {
			FileReader nm = new FileReader(fileName);
			JSONParser parser = new JSONParser();
			JSONObject array = (JSONObject) parser.parse(nm);

			JSONArray vertices = (JSONArray) array.get("vertices");
			for (Object object : vertices) {
				JSONObject vertice = (JSONObject) object;
				JSONObject v = (JSONObject) vertice.get("vertice");

				Long id = (Long) v.get("id");
				Double lat = (Double) v.get("lat");
				Double lon = (Double) v.get("lon");

				mapa.addVertex(id, new Coordinates(lat, lon));
			}

			JSONArray arcos = (JSONArray) array.get("arcos");
			for (Object object : arcos) {
				JSONObject arco = (JSONObject) object;
				JSONObject a = (JSONObject) arco.get("arco");

				Long id = (Long) a.get("id");
				Double dist = (Double) a.get("dist");
				Long id1 = (Long) a.get("idV1");
				Long id2 = (Long) a.get("idV2");

				mapa.addEdge(id1, id2, new Pair<Double,Long>(dist,id) );
			}
			endDocument();
			nm.close();

		} catch (Exception e) {
			System.err.println("Error lectura del JSON");
			mapa = copia;
		}
	}

	public void readJsonVertex( String fileName ) {
		Graph<Long,Coordinates,Pair<Double,Long>> copia = mapa;
		mapa = new Graph<Long, Coordinates, Pair<Double,Long>>();
		try {
			FileReader nm = new FileReader(fileName);
			JSONParser parser = new JSONParser();
			JSONObject array = (JSONObject) parser.parse(nm);

			JSONArray vertices = (JSONArray) array.get("vertices");
			for (Object object : vertices) {
				JSONObject vertice = (JSONObject) object;
				JSONObject v = (JSONObject) vertice.get("vertice");

				Long id = (Long) v.get("id");
				Double lat = (Double) v.get("lat");
				Double lon = (Double) v.get("lon");

				mapa.addVertex(id, new Coordinates(lat, lon));
			}
			nm.close();

		} catch (Exception e) {
			System.err.println("Error lectura del JSON");
			mapa = copia;
		}
	}

	public void readJsonArcs( String fileName ) {
		try {
			FileReader nm = new FileReader(fileName);
			JSONParser parser = new JSONParser();
			JSONObject array = (JSONObject) parser.parse(nm);

			JSONArray arcos = (JSONArray) array.get("arcos");
			for (Object object : arcos) {
				JSONObject arco = (JSONObject) object;
				JSONObject a = (JSONObject) arco.get("arco");

				Long id = (Long) a.get("id");
				Double dist = (Double) a.get("dist");
				Long id1 = (Long) a.get("idV1");
				Long id2 = (Long) a.get("idV2");

				mapa.addEdge(id1, id2, new Pair<Double,Long>(dist,id) );
			}
			endDocument();
			nm.close();

		} catch (Exception e) {
			System.err.println("Error lectura del JSON");
		}
	}



	/**
	 * Metodo auxiliar para la comparaci�n de coordenadas
	 * @param p1 Coordenada 1
	 * @param p2 Coordenada 2
	 * @return Cu�l es mayor
	 */
	public int compareCoordinates(Coordinates p1, Coordinates p2) {

		if(p1.lat> p2.lat) return 1;
		else if(p1.lat < p2.lat) return -1;
		else if(p1.lon > p2.lon) return 1;
		else if(p1.lon < p2.lon) return -1;
		else return 0;
	}

	/**
	 * 
	 * @param p1
	 * @param p2
	 * @return
	 */
	public Double difference(Coordinates p1, Coordinates p2) {
		Double x1 = p1.lat - p2.lat; x1 = x1*x1;
		Double y1 = p1.lon - p2.lon; y1 = y1*y1;
		final int R = 6371; // Radio de la tierra

		Double latDistance = toRad(p2.lat-p1.lat);
		Double lonDistance = toRad(p2.lon-p1.lon);

		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(p1.lat)) * Math.cos(toRad(p2.lat)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);

		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

		Double distance = R * c;
		return distance;
	}

	private static Double toRad(Double value) {
		return value * Math.PI / 180;
	}

	public void delAloneVertex() {
		mapa.delAlone();
		try {
			endDocument();
		} catch (SAXException e) {

		}
	}

	/**
	 * 
	 * @return
	 */
	public Graph<Long, Coordinates, Pair<Double,Long>> getMap(){
		return mapa;
	}

}
