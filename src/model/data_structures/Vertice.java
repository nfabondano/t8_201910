package model.data_structures;

public class Vertice <K extends Comparable<K>,V> implements Comparable<Vertice<K,V>>{

	public K id;

	public V value;

	public ORArray<K> adjLlaves;

	public Vertice(K pId, V pValue) {
		id = pId;
		value = pValue;
		adjLlaves = new ORArray<K>(10);
	}

	public void addAdyacente(K pId) {
		adjLlaves.add(pId);
	}

	public int compareTo(Vertice<K,V> o) {
		return id.compareTo(o.id);
	}

}