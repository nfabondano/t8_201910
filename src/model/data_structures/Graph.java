package model.data_structures;

import java.util.Iterator;

public class Graph<K extends Comparable<K>,V,A> implements IGraph<K,V,A> {

	HashTableLP<K, Vertice<K,V>> vertices;

	HashTableLP<ParVertices<K>, Arco<K,V,A>> arcos;

	public Graph() {
		vertices = new HashTableLP<K, Vertice<K,V>>(5000000);
		arcos = new HashTableLP<ParVertices<K>,Arco<K,V,A>>(900000);
	}

	public int V() {
		return vertices.getSize();
	}

	public int E() {
		return arcos.getSize();
	}

	public void addVertex(K idVertex, V infoVertex) {
		Vertice<K,V> v = new Vertice<K, V>(idVertex, infoVertex);
		vertices.put(idVertex, v);
	}

	public void addEdge(K idVertex, K idVertexFin, A infoArc) {

		Vertice<K,V> v1 = vertices.get(idVertex);
		Vertice<K,V> v2 = vertices.get(idVertexFin);

		v1.addAdyacente(idVertexFin);
		v2.addAdyacente(idVertex);

		Arco<K,V,A> arco = new Arco<K, V, A>(v1, v2, infoArc);
		ParVertices<K> vertices = new ParVertices<K>(idVertex, idVertexFin);
		arcos.put(vertices, arco);
	}

	public V getInfoVertex(K idVertex) {
		Vertice<K,V> v = vertices.get(idVertex);
		if(v!=null)
		return v.value;
		else
			return null;
	}

	public void setInfoVertex(K idVertex, V infoVertex) {
		Vertice<K,V> v = vertices.get(idVertex);
		v.value = infoVertex;
	}

	public A getInfoArc(K idVertexIni, K idVertexFin) {
		ParVertices<K> p = new ParVertices<K>(idVertexIni,idVertexFin);
		Arco<K,V,A> arco = arcos.get(p);
		if(arco != null)
			return arco.info;
		else
			return null;
	}

	public void setInfoArc(K idVertexIni, K idVertexFin, A infoArc) {
		ParVertices<K> p = new ParVertices<K>(idVertexIni,idVertexFin);
		Arco<K,V,A> arco = arcos.get(p);
		arco.info = infoArc;
	}

	public boolean adjVertex( K idVertex ) {
		Vertice<K,V> v = vertices.get(idVertex);
		return (v.adjLlaves.getSize() >= 1);
	}

	public void delAlone() {
		Iterator<K> it = vertices();
		while(it.hasNext()) {
			K key = it.next();
			Vertice<K,V> v = vertices.get(key);	
			if(v.adjLlaves.getSize() == 0) {
				vertices.delete(key);
			}
		}
	}

	public Iterator<K> adj(K idVertex) {
		Vertice<K, V> v = vertices.get(idVertex);
		return v.adjLlaves.iterator();
	}

	public Iterator<K> vertices(){
		return vertices.keys();
	}

	public Iterator<ParVertices<K>> arcos(){
		return arcos.keys();
	}
}
