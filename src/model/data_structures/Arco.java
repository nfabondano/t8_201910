package model.data_structures;

public class Arco<K extends Comparable <K>,V,A> {

	public Vertice<K,V> v1;

	public Vertice<K,V> v2;

	public A info;

	public Arco(Vertice<K,V> pV1, Vertice<K,V> pV2, A inf) {
		v1 = pV1;
		v2 = pV2;
		info = inf;
	}

}