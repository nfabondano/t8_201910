package model.data_structures;

/**
 * Clase auxiliar Pair
 * @param <K> tipo de valor �nico del pair
 */
public class ParVertices<K extends Comparable<K>> implements Comparable<ParVertices<K>>{

	/**
	 * ID del primer vertice
	 */
	public K idV1;

	/**
	 * ID del segundo vertice
	 */
	public K idV2;

	/**
	 * M�todo constructor del pair
	 * @param pKey Valor del primer valor del pair
	 * @param pValue Valor del segundo valor del pair
	 */
	public ParVertices(K pFirst, K pSecond) {
		idV1 = pFirst;
		idV2 = pSecond;
	}

	public int compareTo(ParVertices<K> o) {
		int uno = idV1.compareTo(o.idV1);
		int dos = idV2.compareTo(o.idV2);
		int unodos = idV1.compareTo(o.idV2);
		int dosuno = idV2.compareTo(o.idV1);
		if((unodos == 0 && dosuno == 0))
			return 0;
		if(uno > 0)
			return 1;
		if(uno < 0)
			return -1;
		if(dos > 0)
			return 1;
		if(dos < 0)
			return -1;
		return 0;
	}
	
	public int hashCode() {
		int res = Math.abs((idV1.hashCode() & 0x7fffffff)- (idV2.hashCode() & 0x7fffffff));
		int sum = (idV1.hashCode() & 0x7fffffff) + (idV2.hashCode() & 0x7fffffff);
		return (res + " - " + sum).hashCode();
	}
}
