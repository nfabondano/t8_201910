package view;

import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.PolylineOptions;
import com.teamdev.jxmaps.swing.MapView;

import model.data_structures.Graph;
import model.data_structures.ORArray;
import model.data_structures.Pair;
import model.data_structures.ParVertices;
import model.vo.Coordinates;
import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.LatLngBounds;
import com.teamdev.jxmaps.CircleOptions;

import javax.swing.*;
import java.awt.*;
import java.util.Iterator;

public class MapVisual extends MapView{
	private Graph<Long, Coordinates, Pair<Double,Long>> mapa;
	
	public MapVisual(Graph<Long, Coordinates, Pair<Double,Long>> pMapa) {
		mapa = pMapa;
		setOnMapReadyHandler(new MapReadyHandler() {
            @Override
            public void onMapReady(MapStatus status) {
                // Check if the map is loaded correctly
                if (status == MapStatus.MAP_STATUS_OK) {
                    // Getting the associated map object
                	System.out.println("cargando mapa...Por favor espere");
                    final Map map = getMap();
                    LatLng min = new LatLng(38.8847,-77.0542);
                    LatLng max = new LatLng(38.9135,-76.9976);
                    Iterator<Long> vertices = mapa.vertices();
                    
                    int n = 0;
                    ORArray<Polyline> edges = new ORArray<Polyline>();
                    Iterator<ParVertices<Long>> arcos = mapa.arcos();
                    PolylineOptions options = new PolylineOptions();
                    // Setting geodesic property value
                    options.setGeodesic(true);
                    // Setting stroke color value
                    options.setStrokeColor("#3352FF");
                    // Setting stroke opacity value
                    options.setStrokeOpacity(1.0);
                    // Setting stroke weight value
                    options.setStrokeWeight(2.0);
                    // Applying options to the polyline
                    
                    CircleOptions op = new CircleOptions();
                    op.setFillColor("#FF0000");
                    op.setStrokeColor("#FF0000");
                    //op.setStrokeOpacity(1.0);
                    op.setRadius(2);
                    ORArray<Circle> circulos = new ORArray<Circle>();
                    while(arcos.hasNext()) {
                    	ParVertices<Long> val = arcos.next();
                    	double lat1 = mapa.getInfoVertex(val.idV1).lat;
                    	double lon1 = mapa.getInfoVertex(val.idV1).lon;
                    	double lat2 = mapa.getInfoVertex(val.idV2).lat;
                    	double lon2 = mapa.getInfoVertex(val.idV2).lon;
                    	if(n < 3000 && lat1 >= 38.8847 && lat1 <= 38.9135 && lon1 >= -77.0542 && lon1 <= -76.9976
                    			&& lat2 >= 38.8847 && lat2 <= 38.9135 && lon2 >= -77.0542 && lon2 <= -76.9976) {
                    		LatLng[] path = {new LatLng(lat1,lon1),new LatLng(lat2,lon2)};
                    		edges.add(new Polyline(map));
                        	edges.getElement(edges.getSize()-1).setPath(path);
                        	edges.getElement(edges.getSize()-1).setOptions(options);
                        	circulos.add(new Circle(map));
                        	circulos.getElement(circulos.getSize()-1).setCenter(new LatLng(lat1,lon1));
                        	circulos.getElement(circulos.getSize()-1).setOptions(op);
                        	circulos.add(new Circle(map));
                        	circulos.getElement(circulos.getSize()-1).setCenter(new LatLng(lat2,lon2));
                        	circulos.getElement(circulos.getSize()-1).setOptions(op);
                        	n++;
                    	}
                    }
                    System.out.println("cargo el mapa");
                    map.fitBounds(new LatLngBounds(min,max));
                    // Setting initial zoom value
                    map.setZoom(12.5);
                }
            }
		});
	}
	public static void main(String[] args) {
        final MapVisual sample = new MapVisual(null);
        System.out.println("entro aqui?");
        JFrame frame = new JFrame("Polylines");

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(sample, BorderLayout.CENTER);
        frame.setSize(700, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
