package view;

import java.util.Iterator;

import model.vo.*;

public class MapManagerView {
	
	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 8----------------------");
		System.out.println("0. Cargar datos del archivo OPENMAP de Central Washington");
		System.out.println("1. Guardar los datos del OPENMAP en archivo JSON");
		System.out.println("2. Leer archivo JSON del OPENMAP");
		System.out.println("3. Pintar grafo OPENMAP");
		System.out.println("4. Leer map.xml y crear grafo resultante");
		System.out.println("5. Escribir JSON de map.xml");
		System.out.println("6. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
	
	public void printMessage(String mensaje) {
		System.out.println(mensaje);
	}
	
}
